import torch
import torch.nn as nn
import torch.nn.functional as F

class CIFAR10Network(nn.Module):

    def __init__(self, numClasses, imgSize):

        super().__init__()

        self.img_size = imgSize

        self.conv1_i = 1
        self.conv1_f = 6
        self.conv1_k = 5
        self.conv2_i = 6
        self.conv2_f = 16
        self.conv2_k = 5
        self.fc_in = int((((imgSize - self.conv1_k + 1) / 2) - self.conv2_k + 1) / 2)

        print(self.fc_in)

        self.conv1  = nn.Conv2d(self.conv1_i, self.conv1_f, self.conv1_k)
        self.pool   = nn.MaxPool2d(2, 2)
        self.conv2  = nn.Conv2d(self.conv2_i, self.conv2_f, self.conv2_k)
        self.fc1    = nn.Linear(16 * self.fc_in * self.fc_in, 120)
        self.fc2    = nn.Linear(120, 84)
        self.fc3    = nn.Linear(84, numClasses)

    def __repr__(self):

        return 'CIFAR10 Network...'

    def forward(self, x):

        x = self.conv1(x)
        x = F.relu(x)
        x = self.pool(x)
        x = self.conv2(x)
        x = F.relu(x)
        x = self.pool(x)
        x = x.view(-1, 16 * self.fc_in * self.fc_in)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)

        return x
