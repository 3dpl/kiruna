import network.cifar10_network

networks = { 'cifar10' : network.cifar10_network.CIFAR10Network }

def get_network(name, numClasses, imgSize):
    
    return networks[name](numClasses, imgSize)
