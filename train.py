import argparse
import datetime
import logging
import sys

import tensorboardX
import torch
import torch.autograd
import torchvision
import torchvision.transforms as transforms

import loader.utils
import network.utils

log = logging.getLogger(__name__)

def evaluate(args, network, loader, numImages, batchSize, classes, numClasses):

    network.eval()

    class_correct_ = list(0.0 for i in range(numClasses))
    class_total_ = list(0.0 for i in range(numClasses))

    with torch.no_grad():

        for data in loader:

            inputs_, labels_ = data
            inputs_ = torch.autograd.Variable(inputs_.cuda())
            labels_ = torch.autograd.Variable(labels_.cuda())

            N_ = labels_.size(0)

            outputs_ = network(inputs_)

            _, predicted_ = torch.max(outputs_, 1)
            c_ = (predicted_ == labels_).squeeze()

            for i in range(N_):

                label_ = labels_[i]
                class_correct_[label_] += c_[i].item()
                class_total_[label_] += 1

    for i in range(numClasses):

        log.info('Accuracy of class {0} : {1} %'.format(
                    classes[i], 100.0 * class_correct_[i] / class_total_[i]))

    log.info('Accuracy : {0} %'.format((100.0 * sum(class_correct_) / sum(class_total_))))

def train(args):

    # Set up visualization and experiment
    log.info('Setting up visualization and experiment...')
    experiment_str_ = '{0}-{1}-{2}'.format(args.network,
                                           args.dataset,
                                           datetime.datetime.now().strftime('%b%d_%H-%M-%S'))
    writer_ = tensorboardX.SummaryWriter('experiments/logs/' + experiment_str_)

    # Set up data augmentation and transformations
    transform_ = transforms.Compose(
            [transforms.ToTensor(),
             transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    # Set up dataset and loaders
    data_path_ = loader.utils.get_path(args.dataset)
    train_dataset_ = loader.utils.get_loader(args.dataset)(data_path_, 'train', args.img_width, args.img_height, isTransform=True)
    log.info(train_dataset_)
    train_loader_ = torch.utils.data.DataLoader(train_dataset_, batch_size=args.batch_size, shuffle=True, num_workers=2)

    classes_ = train_dataset_.classes
    num_classes_ = train_dataset_.num_classes

    # Network loader
    network_ = network.utils.get_network(args.network, num_classes_, args.img_width)
    log.info(network_)
    network_ = torch.nn.DataParallel(network_, device_ids=range(torch.cuda.device_count()))

    # Set up optimizer
    optimizer_ = torch.optim.SGD(network_.parameters(),
                                 lr=args.learning_rate,
                                 momentum=0.9)

    # Set up loss
    criterion_ = torch.nn.CrossEntropyLoss()
    criterion_.cuda()

    # Train the network
    for epoch in range(args.epochs):

        log.info('*** Epoch {0} ***'.format(epoch))
        network_.train()

        for i, data in enumerate(train_loader_, 0):

            inputs_, labels_ = data


            N_ = inputs_.size(0)

            inputs_ = torch.autograd.Variable(inputs_.cuda())
            labels_ = torch.autograd.Variable(labels_.cuda())
            #log.info(inputs_.size())

            optimizer_.zero_grad()
            outputs_ = network_(inputs_)
            loss_ = criterion_(outputs_, labels_)

            # Visualization
            n_iter_ = epoch * len(train_dataset_) / args.batch_size + i + 1
            writer_.add_scalar('Loss', loss_.item(), n_iter_)
            log.info('[{0},{1}] loss: {2}'.format(
                        epoch,
                        i,
                        loss_.item()/N_))

            vis_inputs_ = torchvision.utils.make_grid(inputs_, normalize=True, scale_each=False)
            writer_.add_image('Inputs', vis_inputs_, n_iter_)

            # Optimization
            loss_.backward()
            optimizer_.step()

        if epoch % args.evaluate == 0:

            log.info('=== Evaluating on training set ===')
            evaluate(args, network_, train_loader_, len(train_dataset_), args.batch_size, classes_, num_classes_)

if __name__ == '__main__':

    logging.basicConfig(stream=sys.stdout, level=logging.INFO)

    parser_ = argparse.ArgumentParser(description='Parameters')
    parser_.add_argument('--network', nargs='?', type=str, default='cifar10',
                            help='The network to train with')
    parser_.add_argument('--dataset', nargs='?', type=str, default='base',
                            help='The dataset to train with')
    parser_.add_argument('--learning_rate', nargs='?', type=float, default=1e-3,
                            help='Starting learning rate for the optimizer')
    parser_.add_argument('--batch_size', nargs='?', type=int, default=32,
                            help='Batch size for training/testint')
    parser_.add_argument('--epochs', nargs='?', type=int, default=100,
                            help='Number of epochs')
    parser_.add_argument('--evaluate', nargs='?', type=int, default=1,
                            help='Evaluate each certain epochs')
    parser_.add_argument('--img_width', nargs='?', type=int, default=256,
                            help='Image width')
    parser_.add_argument('--img_height', nargs='?', type=int, default=256,
                            help='Image height')

    args_ = parser_.parse_args()

    train(args_)
