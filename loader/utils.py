import os

import loader.base_loader

loaders_ = { 'base' : loader.base_loader.BaseLoader }

paths_ = { 'base' : 'datasets/base' }

def get_loader(name):
    return loaders_[name]

def get_path(name):
    return paths_[name]

def recursive_glob(root='.', suffix=''):
    files_ = [os.path.join(dirpath, filename)
                for dirpath, _, filenames in os.walk(root)
                for filename in filenames if filename.endswith(suffix)]
    return files_
