import collections
import logging
import os

import numpy as np
import scipy.misc
import torch
import torch.utils.data
import torchvision

import loader.utils

class BaseLoader(torch.utils.data.Dataset):

    def __init__(self, root, split, imgWidth, imgHeight, imgNorm=True, isTransform=False):

        self.logger = logging.getLogger(__name__)

        self.root = root
        self.split = split
        self.img_size = (imgWidth, imgHeight)
        self.img_norm = imgNorm
        self.is_transform = isTransform

        self.num_classes = 13
        self.classes = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'ia', 'ib', 'j', 'k', 'm']
        self.class_map = dict(zip(self.classes, range(self.num_classes)))

        self.images_base_path = os.path.join(self.root, self.split)
        self.files = loader.utils.recursive_glob(root=self.images_base_path, suffix='.png')

        if not self.files:
            raise Exception("No files for the requested split {0} were found in {1}".format(
                                self.split,
                                self.images_base_path))

        self.logger.debug("Found {0} images in split {1} on {2}".format(
                len(self.files),
                self.split,
                self.images_base_path))

    def __len__(self):

        return len(self.files)

    def __getitem__(self, index):

        img_path_ = self.files[index].rstrip()

        lbl_ = None
        for c in self.classes:
            if ('/' + c + '/') in img_path_:
                lbl_ = self.class_map[c]
                break

        self.logger.debug("Loading image {0} with label {1}".format(img_path_, self.classes[lbl_]))

        img_ = scipy.misc.imread(img_path_)
        img_ = np.array(img_, dtype=np.uint8)
        #self.logger.info(img_.shape)

        lbl_ = np.array(lbl_, dtype=np.long)

        if self.is_transform:
            img_, lbl_ = self.transform(img_, lbl_)

        return img_, lbl_
    
    def __repr__(self):

        return "Dataset loader for Cityscapes {0} split with {1} images in {2}...".format(
                    self.split,
                    self.__len__(),
                    self.images_base_path)

    def transform(self, img, lbl):

        img_ = scipy.misc.imresize(img, self.img_size)
        img_ = np.expand_dims(img_, axis=2)
        img_ = img_[:,:,::-1]
        img_ = img_.astype(np.float64)
        if self.img_norm:
            img_ = img_.astype(float) / 255.0
        img_ = img_.transpose(2, 0, 1)
        #self.logger.info(img_.shape)

        img_ = torch.from_numpy(img_).float()
        lbl_ = torch.from_numpy(lbl).long()

        return img_, lbl_
